#!/bin/bash

set -e

WORKSPACE=/srv/janitor/debian-janitor

export SBUILD_CONFIG=/srv/janitor/sbuildrc

$WORKSPACE/pull_worker.sh \
    --base-url=https://janitor.debian.net/api/ \
    --build-command="sbuild -A -v -c unstable-amd64-sbuild" \
    --credentials=/srv/janitor/janitor.creds.json
